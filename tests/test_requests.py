# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import pytest

from sftpnotary.core import SingleFileRequest
from sftpnotary.exceptions import InvalidFileName


class TestRequests:
    def test_SingleFileRequest_validation(self):
        validFileName = SingleFileRequest(fileName="foo.apk")
        validFileName.validate()

        missingFileName = SingleFileRequest()
        with pytest.raises(InvalidFileName):
            missingFileName.validate()

        emptyFileName = SingleFileRequest(fileName="")
        with pytest.raises(InvalidFileName):
            emptyFileName.validate()

        invalidFileName = SingleFileRequest(fileName="../../etc/passwd")
        with pytest.raises(InvalidFileName):
            invalidFileName.validate()

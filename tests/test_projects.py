# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import pytest

from sftpnotary import projects


class TestProjects:
    def test_load_branch_settings(self):
        settings = """
            namespace/project:
              branches:
                branchname:
                  keyA: valueA
                  keyB: valueB
        """
        projects.readProjectSettings(settings)
        assert projects.settings is not None
        assert "namespace/project" in projects.settings._projects
        p = projects.settings._projects["namespace/project"]
        assert "branchname" in p
        b = p["branchname"]
        assert len(b) == 2
        assert b.get("keyA") == "valueA"
        assert b.get("keyB") == "valueB"

    def test_load_project_and_branch_settings(self):
        settings = """
            namespace/project:
              keyA: valueAP
              keyC: valueC
              branches:
                branchA:
                  keyA: valueA
                  keyB: valueB
                branchB:
        """
        projects.readProjectSettings(settings)
        assert projects.settings is not None
        assert "namespace/project" in projects.settings._projects
        p = projects.settings._projects["namespace/project"]
        assert len(p) == 2
        branchA = p["branchA"]
        assert len(branchA) == 3
        assert branchA.get("keyA") == "valueA"
        assert branchA.get("keyB") == "valueB"
        assert branchA.get("keyC") == "valueC"
        branchB = p["branchB"]
        assert len(branchB) == 2
        assert branchB.get("keyA") == "valueAP"
        assert branchB.get("keyC") == "valueC"

    def test_load_default_settings(self):
        settings = """
            defaults:
              keyA: defaultA
              keyC: defaultC
              branches:
              - branchname

            namespace/project:
        """
        projects.readProjectSettings(settings)
        assert projects.settings is not None
        assert "namespace/project" in projects.settings._projects
        p = projects.settings._projects["namespace/project"]
        assert "branchname" in p
        b = p["branchname"]
        assert len(b) == 2
        assert b.get("keyA") == "defaultA"
        assert b.get("keyC") == "defaultC"

    def test_load_settings(self):
        settings = """
            defaults:
              keyA: defaultA
              keyC: defaultC
              keyD: defaultD
              branches:
              - defaultBranch

            namespace/project:
              keyA: projectA
              keyB: projectB
              branches:
                branchA:
                  keyA: valueA
                defaultBranch:
                  keyC: valueC
        """
        projects.readProjectSettings(settings)
        assert projects.settings is not None
        assert "namespace/project" in projects.settings._projects
        p = projects.settings._projects["namespace/project"]
        assert len(p) == 2
        assert "branchA" in p
        branchA = p["branchA"]
        assert len(branchA) == 4
        assert branchA.get("keyA") == "valueA"
        assert branchA.get("keyB") == "projectB"
        assert branchA.get("keyC") == "defaultC"
        assert branchA.get("keyD") == "defaultD"
        assert "defaultBranch" in p
        defaultBranch = p["defaultBranch"]
        assert len(defaultBranch) == 4
        assert defaultBranch.get("keyA") == "projectA"
        assert defaultBranch.get("keyB") == "projectB"
        assert defaultBranch.get("keyC") == "valueC"
        assert defaultBranch.get("keyD") == "defaultD"

    def test_branches_looking_like_int_float_bool_etc(self):
        settings = """
            namespace/projectA:
              branches:
                42:
                2.0:
                true:
        """
        projects.readProjectSettings(settings)
        assert projects.settings is not None
        assert "namespace/projectA" in projects.settings._projects
        p = projects.settings._projects["namespace/projectA"]
        assert len(p) == 3
        assert "42" in p
        assert "2.0" in p
        assert "true" in p

    def test_exists(self):
        settings = """
            defaults:
              branches:
              - defaultBranch

            namespace/project:
              branches:
                branchA:
        """
        projects.readProjectSettings(settings)
        assert projects.settings.exists("namespace/project")
        assert not projects.settings.exists("namespace/other-project")

        assert projects.settings.exists("namespace/project", "branchA")
        assert projects.settings.exists("namespace/project", "defaultBranch")
        assert not projects.settings.exists("namespace/project", "otherBranch")

    def test_get(self):
        settings = """
            defaults:
              keyA: defaultA
              keyC: defaultC
              branches:
              - defaultBranch

            namespace/project:
              keyA: projectA
              keyB: projectB
              branches:
                branchA:
                  keyA: valueA
        """
        projects.readProjectSettings(settings)
        assert projects.settings.get("namespace/project", "branchA", "keyA") == "valueA"
        assert projects.settings.get("namespace/project", "branchA", "keyB") == "projectB"
        assert projects.settings.get("namespace/project", "branchA", "keyC") == "defaultC"
        assert projects.settings.get("namespace/project", "defaultBranch", "keyA") == "projectA"
        assert projects.settings.get("namespace/project", "defaultBranch", "keyB") == "projectB"
        assert projects.settings.get("namespace/project", "defaultBranch", "keyC") == "defaultC"

        with pytest.raises(projects.Error, match=r"Unknown project .*"):
            projects.settings.get("namespace/other-project", "defaultBranch", "keyA")
        with pytest.raises(projects.Error, match=r"Unknown branch .*"):
            projects.settings.get("namespace/project", "otherBranch", "keyA")
        assert projects.settings.get("namespace/project", "branchA", "unknownKey") is None
        assert projects.settings.get("namespace/project", "branchA", "unknownKey", ["foo"]) == ["foo"]

    def test_getBool(self):
        settings = """
            namespace/project:
              branches:
                branchA:
                  boolTrue: tRuE
                  bool1: 1
                  boolYes: YeS
                  boolOn: oN
                  boolFalse: fAlsE
                  bool0: 0
                  boolNo: nO
                  boolOff: oFF
                  boolInvalid: invalid
        """
        projects.readProjectSettings(settings)
        assert projects.settings.getBool("namespace/project", "branchA", "boolTrue", False) is True
        assert projects.settings.getBool("namespace/project", "branchA", "bool1", False) is True
        assert projects.settings.getBool("namespace/project", "branchA", "boolYes", False) is True
        assert projects.settings.getBool("namespace/project", "branchA", "boolOn", False) is True
        assert projects.settings.getBool("namespace/project", "branchA", "boolFalse", True) is False
        assert projects.settings.getBool("namespace/project", "branchA", "bool0", True) is False
        assert projects.settings.getBool("namespace/project", "branchA", "boolNo", True) is False
        assert projects.settings.getBool("namespace/project", "branchA", "boolOff", True) is False

        assert projects.settings.getBool("namespace/project", "branchA", "boolNoSet", True) is True
        assert projects.settings.getBool("namespace/project", "branchA", "boolNoSet", False) is False

        with pytest.raises(ValueError, match=r"Not a boolean: .*"):
            projects.settings.getBool("namespace/project", "branchA", "boolInvalid", True)

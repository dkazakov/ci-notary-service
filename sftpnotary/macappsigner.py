# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
# SPDX-FileCopyrightText: 2023 Julius Künzel <jk.kdedev@smartlab.uber.space>

import logging
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

import sftpnotary.log
from sftpnotary import appleutils, config, projects, util
from sftpnotary.core import SignFileResponse, SingleFileRequest, Worker
from sftpnotary.sftp import SFTPClient

log = logging.getLogger(__name__)

SignMacAppRequest = SingleFileRequest
SignMacAppResponse = SignFileResponse


class SignMacAppJob(appleutils.AppleJob):
    def __init__(
        self,
        sftp: SFTPClient,
        macAppFilePath: Path,
        token: Optional[str] = None,
        projectPath: Optional[str] = None,
        branch: Optional[str] = None,
    ):
        super().__init__(sftp, macAppFilePath)

        self.request = SignMacAppRequest(
            fileName=macAppFilePath.name, token=token, projectPath=projectPath, branch=branch
        )
        self.responseClass = SignMacAppResponse


class SignMacAppProcessor(appleutils.AppleTaskProcessor):
    requestClass = SignMacAppRequest
    responseClass = SignMacAppResponse
    resultNameSuffix = "signed"

    def doProcess(self):
        remoteFilePath = self.task.path() / self.request.fileName

        with TemporaryDirectory() as workPathName:
            workPath = Path(workPathName)

            localFilePath = self.downloadApp(workPath, remoteFilePath)
            localSignedFilePath = workPath / f"processed{localFilePath.suffix}"

            allowedApplicationId = projects.settings.get(self.request.projectPath, self.request.branch, "applicationid")
            appleutils.verifyApplicationId(localFilePath, allowedApplicationId)
            self.signMacApp(localFilePath, localSignedFilePath.name)

            return self.uploadApp(workPath, localSignedFilePath, remoteFilePath, localFilePath)

    def signMacApp(self, sourceFilePath, signedFileName):
        workingDirectory = sourceFilePath.parent
        sourceFileName = sourceFilePath.name
        rcodesignPath = config.settings.getPath("AppleTools", "RCodesignPath") or "rcodesign"

        keystore = config.settings.getPath("MacAppSigning", "KeyStore")
        keystorepass = config.settings.get("MacAppSigning", "KeyStorePass")
        command = [rcodesignPath, "sign", "-v"]
        if keystore and keystore.suffix in [".pem", ".crt", ".key"]:
            command += ["--pem-file", Path(keystore)]
        elif keystore and keystore.suffix in [".p12", ".pfx"]:
            command += ["--p12-file", Path(keystore)]
            if keystorepass:
                if keystorepass.startswith("file:"):
                    keystorepass = Path(keystorepass[5:]).expanduser()
                    command += ["--p12-password-file", keystorepass]
                else:
                    command += ["--p12-password", keystorepass]
        command += [sourceFileName, signedFileName]
        commandToLog = sftpnotary.log.maskFollowingItem(command, "--p12-password")
        util.runCommand(command, commandToLog=commandToLog, cwd=workingDirectory)


class SignMacAppWorker(Worker):
    processorClass = SignMacAppProcessor

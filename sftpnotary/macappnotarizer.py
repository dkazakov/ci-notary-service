# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
# SPDX-FileCopyrightText: 2023 Julius Künzel <jk.kdedev@smartlab.uber.space>

import logging
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import Optional

import sftpnotary.log
from sftpnotary import appleutils, config, projects, util
from sftpnotary.core import SignFileResponse, SingleFileRequest, Worker
from sftpnotary.sftp import SFTPClient

log = logging.getLogger(__name__)

NotarizeMacAppRequest = SingleFileRequest
NotarizeMacAppResponse = SignFileResponse


class NotarizeMacAppJob(appleutils.AppleJob):
    def __init__(
        self,
        sftp: SFTPClient,
        macAppFilePath: Path,
        token: Optional[str] = None,
        projectPath: Optional[str] = None,
        branch: Optional[str] = None,
    ):
        super().__init__(sftp, macAppFilePath)

        self.request = NotarizeMacAppRequest(
            fileName=macAppFilePath.name, token=token, projectPath=projectPath, branch=branch
        )
        self.responseClass = NotarizeMacAppResponse


class NotarizeMacAppProcessor(appleutils.AppleTaskProcessor):
    requestClass = NotarizeMacAppRequest
    responseClass = NotarizeMacAppResponse
    resultNameSuffix = "notarized"

    def doProcess(self):
        remoteFilePath = self.task.path() / self.request.fileName

        with TemporaryDirectory() as workPathName:
            workPath = Path(workPathName)

            localFilePath = self.downloadApp(workPath, remoteFilePath)

            allowedApplicationId = projects.settings.get(self.request.projectPath, self.request.branch, "applicationid")
            appleutils.verifyApplicationId(localFilePath, allowedApplicationId)
            self.notarizeMacApp(localFilePath)

            return self.uploadApp(workPath, localFilePath, remoteFilePath)

    def notarizeMacApp(self, filePath):
        workingDirectory = filePath.parent
        fileName = filePath.name

        rcodesignPath = config.settings.getPath("AppleTools", "RCodesignPath") or "rcodesign"

        command = [rcodesignPath, "notary-submit", "-v"]

        apiKey = config.settings.get("MacAppNotarization", "ApiKey")
        if apiKey.startswith("file:"):
            apiKey = Path(apiKey[5:]).expanduser()
            command += ["--api-key-file", apiKey]
        else:
            command += ["--api-key", apiKey]

        command += ["--staple", fileName]
        commandToLog = sftpnotary.log.maskFollowingItem(command, "--api-key")
        util.runCommand(command, commandToLog=commandToLog, cwd=workingDirectory)


class NotarizeMacAppWorker(Worker):
    processorClass = NotarizeMacAppProcessor
